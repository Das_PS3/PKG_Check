﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PKG_Check
{
    class Check_PKG
    {
        public event EventHandler HashingProgress;
        Check_PKGEventArgs check_PKGEventArgs;

        public void hashingStarted(Check_PKGEventArgs e)
        {
            EventHandler hashingProgress = HashingProgress;
            if (hashingProgress != null)
                hashingProgress(this, e);
        }

        const uint MIB = 1024 * 1024;

        public string CheckSize(string file, bool log = false)
        {
            byte[] totalSizeArray;
            ulong sizeReal;

            short MAX_COLUMN;
            try
            { 
                MAX_COLUMN = (short)(Console.WindowWidth - 1); // Constant for the string wrapper's column width 
            }
            catch (IOException) { MAX_COLUMN = 80; }

            using (BinaryReader pkgFileReader = new BinaryReader(File.OpenRead(file)))
            {
                // Head to 0x18 and read the size (0x8 bytes long)
                pkgFileReader.BaseStream.Seek(0x18, SeekOrigin.Begin);
                totalSizeArray = pkgFileReader.ReadBytes(0x8);

                sizeReal = (ulong)pkgFileReader.BaseStream.Length;
            }

            // Most consumer CPUs are LE, while the size is read in BE. If this CPU is LE, reverse the bytes array
            if (BitConverter.IsLittleEndian)
                Array.Reverse(totalSizeArray);

            ulong sizeReported = BitConverter.ToUInt64(totalSizeArray, 0);

            double sizeReportedMiB = (double)sizeReported / MIB;
            double sizeRealMiB = (double)sizeReal / MIB;

            if (!log)
            {
                if (sizeReported != sizeReal)
                    return Misc_Utils.StringWrapper(String.Format("Error: Check FAILED on {0}.\n", file), MAX_COLUMN) + "The filesize is incorrect.\n\n" +
                        Misc_Utils.StringWrapper(String.Format("Expected {0:#,#} bytes ({1:#,0.#} MiB), found {2:#,#} bytes ({3:#,0.#} MiB)", sizeReported, sizeReportedMiB, sizeReal, sizeRealMiB), MAX_COLUMN);
                return Misc_Utils.StringWrapper("Filesize OK. The hash will now be verified. Do NOT close the application until it's done!", MAX_COLUMN);
            }
            if (sizeReported != sizeReal)
            {
                string logFile = file.Substring(0, file.Length - 4) + "_BAD_SIZE.txt";
                using (StreamWriter logger = new StreamWriter(logFile, false, Encoding.UTF8))
                {
                    logger.WriteLine("The filesize is incorrect.\n\nExpected {0:#,#} bytes ({1:#,0.#} MiB), found {2:#,#} bytes ({3:#,0.#} MiB)", sizeReported, sizeReportedMiB, sizeReal, sizeRealMiB);
                    return "Error";
                }
            }
            return String.Empty;
        }

        public string CheckPKG(string file, bool printConsoleProgressBar, bool log=false, string expectedHash = null)
        {
            string builtInHash, hashResult;
            ulong size;

            string truncatedFilename = Path.GetFileNameWithoutExtension(file);

            if (truncatedFilename.Length > 42)
                truncatedFilename = Misc_Utils.Truncate(truncatedFilename, 42);

            using (BinaryReader pkgFileReader = new BinaryReader(File.Open(file, FileMode.Open, FileAccess.ReadWrite)))
            {
                if (String.IsNullOrEmpty(expectedHash))
                {
                    // Read the last 20 bytes
                    byte[] rawHash = new byte[0x14];
                    pkgFileReader.BaseStream.Seek(-0x20, SeekOrigin.End);
                    rawHash = pkgFileReader.ReadBytes(0x14);

                    // Finally, obtain the SHA-1 hash contained within the PKG
                    builtInHash = BitConverter.ToString(rawHash).Replace("-", "");
                }
                else
                    builtInHash = expectedHash.ToUpper();

                size = (ulong)pkgFileReader.BaseStream.Length;
            }

            using (SHA1 hasher = SHA1.Create())
            using (FileStream input = File.OpenRead(file))
            {
                double previousPercent = 0.00;

                byte[] bytesRead = new byte[MIB * 8]; // 8 MiB buffer
                int offset;
                ulong progress = 0; // These are the total bytes read, ulong for files >2GiB

                // Finally, begin the hashing. After it's complete (0 bytes read), it has to be finalized with 0 bytes to read from offset 0
                while ((offset = input.Read(bytesRead, 0, bytesRead.Length)) > 0)
                {
                    progress += (ulong)offset;

                    if (progress >= (ulong)(input.Length - 0x20))
                        offset -= (int)(progress - (ulong)(input.Length - 0x20));

                    hasher.TransformBlock(bytesRead, 0, offset, bytesRead, 0);

                    int progressPercent = (int)((progress * 100) / size);
                    check_PKGEventArgs = new Check_PKGEventArgs(progressPercent);
                    hashingStarted(check_PKGEventArgs);

                    if (printConsoleProgressBar)
                        previousPercent = Misc_Utils.PrintProgress(progress, size, previousPercent, truncatedFilename);
                }
                hasher.TransformFinalBlock(bytesRead, 0, 0);

                if (printConsoleProgressBar)
                    Console.WriteLine();

                hashResult = BitConverter.ToString(hasher.Hash).Replace("-", "");
            }

            if (!log)
            {
                if (builtInHash == hashResult)
                    return String.Format("\n\nHash OK on {0}", file);
                return String.Format("\n\n{0," + Console.WindowWidth / 2 + "}\n\nExpected: {1,15}\nObtained: {2,15}", "BAD HASH", builtInHash, hashResult);
            }
            string logFile;

            if (builtInHash == hashResult)
                logFile = file.Substring(0, file.Length - 4) + "_HASH_OK.txt";
            else
                logFile = file.Substring(0, file.Length - 4) + "_BAD_HASH.txt";
            using(StreamWriter logger = new StreamWriter(logFile, false, Encoding.UTF8))
            {
                if (builtInHash.ToUpper() == hashResult.ToUpper())
                    logger.WriteLine("Hash OK");
                else
                    logger.WriteLine("BAD HASH\n\nExpected: {0,15}\nObtained: {1,15}", builtInHash, hashResult);
            }

            return String.Empty;
        }
    }


    class Check_PKGEventArgs : EventArgs
    {
        public int Progress { get; private set; }

        public Check_PKGEventArgs(int progress)
        {
            Progress = progress;
        }
    }
}
