﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace PKG_Check
{
    internal class Program
    {
        #region "Unmanaged"

        [DllImport("user32.dll")]
        private static extern bool EnableMenuItem(IntPtr hMenu, uint uIDEnableItem, uint uEnable);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32.dll")]
        private static extern IntPtr RemoveMenu(IntPtr hMenu, uint nPosition, uint wFlags);

        internal const uint SC_RESTORE = 0xF120;
        internal const uint SC_CLOSE = 0xF060;
        internal const uint MF_GRAYED = 0x00000001;
        internal const uint MF_BYCOMMAND = 0x00000000;

        #endregion "Unmanaged"

        private const string KEY_EXIT = "\nPress any key to exit...";
        private const string MAGIC = "7F-50-4B-47";
        private const string TARGET_FOLDER = "target";

        private static void Main(string[] args)
        {
            switch (args.Length)
            {
                case 0:
                    Pkg_Processor(TARGET_FOLDER);
                    break;

                default:
                    Single_Run(args[0]);
                    break;
            }
        }

        private static void Single_Run(string file)
        {
            IntPtr hMenu = Process.GetCurrentProcess().MainWindowHandle;
            Check_PKG checker = new Check_PKG();

            // Make sure a file has been passed and that the passed file is a valid PKG
            if (!file.EndsWith(".pkg", true, null))
            {
                Console.WriteLine("Eh. Drag and drop a PKG on this application, or something...");
                Console.ReadKey(true);
                Environment.Exit(0);
            }

            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                byte[] magic = reader.ReadBytes(0x4);

                if (BitConverter.ToString(magic) != MAGIC) // PSP/PS3/PSV PKG
                {
                    Console.WriteLine("Error. This is not a valid PS3, PSP or PSVita PKG file.");
                    Console.ReadKey(true);
                    Environment.Exit(0);
                }
            }

            string result = checker.CheckSize(file);
            if (result.Contains("Error"))
            {
                Console.WriteLine(result);
                Console.WriteLine(KEY_EXIT);
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
                Console.WriteLine(result);

            IntPtr hSystemMenu = GetSystemMenu(hMenu, false); // This bool will disable the close button and the exit system menu option
            EnableMenuItem(hSystemMenu, SC_CLOSE, MF_GRAYED);
            RemoveMenu(hSystemMenu, SC_CLOSE, MF_BYCOMMAND);
            Console.TreatControlCAsInput = true;

            result = checker.CheckPKG(file, true); // Now that the window is "safe" from the user accidentally closing it, begin hashing
            Console.WriteLine(result);

            hSystemMenu = GetSystemMenu(hMenu, true); // Re-enable the close button and exist system menu option
            EnableMenuItem(hSystemMenu, SC_CLOSE, MF_GRAYED);
            RemoveMenu(hSystemMenu, SC_CLOSE, MF_BYCOMMAND);
            Console.TreatControlCAsInput = false;

            Console.WriteLine(KEY_EXIT);
            Console.ReadKey();
        }

        private static void Pkg_Processor(string targetFolder)
        {
            if (!Directory.Exists(targetFolder))
                Directory.CreateDirectory(targetFolder);

            uint count = 0;
            string result;
            string logFile;

            IntPtr hMenu = Process.GetCurrentProcess().MainWindowHandle;
            IntPtr hSystemMenu = GetSystemMenu(hMenu, false); // This bool will disable the close button and the exit system menu option
            EnableMenuItem(hSystemMenu, SC_CLOSE, MF_GRAYED);
            RemoveMenu(hSystemMenu, SC_CLOSE, MF_BYCOMMAND);
            Console.TreatControlCAsInput = true;

            Check_PKG checker = new Check_PKG();

            IEnumerable<string> pkgFileList = Directory.EnumerateFiles(targetFolder, "*.pkg", SearchOption.AllDirectories);

            foreach (string pkgFile in pkgFileList)
            {
                count++;

                logFile = pkgFile.Substring(0, pkgFile.Length - 4);

                if (File.Exists(logFile + "_HASH_OK.txt"))
                    continue;

                if (File.Exists(logFile + "_BAD_SIZE.txt"))
                    File.Delete(logFile + "_BAD_SIZE.txt");

                if (File.Exists(logFile + "_BAD_HASH.txt"))
                    File.Delete(logFile + "_BAD_HASH.txt");

                if (File.Exists(logFile + "_INVALID_PKG.txt"))
                    File.Delete(logFile + "_INVALID_PKG.txt");

                using (FileStream stream = new FileStream(pkgFile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    byte[] magic = reader.ReadBytes(0x4);

                    if (BitConverter.ToString(magic) != "7F-50-4B-47") // PSP/PS3/PSV PKG
                    {
                        using (TextWriter writer = File.CreateText(logFile + "_INVALID_PKG.txt"))
                            writer.WriteLine("This is not a valid PS3, PSP or PSVita PKG file.");

                        continue;
                    }
                }

                result = checker.CheckSize(pkgFile, true);

                if (!File.Exists(logFile + "_BAD_SIZE.txt"))
                    checker.CheckPKG(pkgFile, true, true);
            }

            hSystemMenu = GetSystemMenu(hMenu, true); // Re-enable the close button and exist system menu option
            EnableMenuItem(hSystemMenu, SC_CLOSE, MF_GRAYED);
            RemoveMenu(hSystemMenu, SC_CLOSE, MF_BYCOMMAND);
            Console.TreatControlCAsInput = false;

            if (count == 0)
            {
                Console.WriteLine("No PKG file detected.");
                Console.WriteLine(KEY_EXIT);
                Console.ReadKey();
            }
        }
    }
}
